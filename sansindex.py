import html 
import webbrowser
import csv
import sys
from string import ascii_letters
from typing import List
from pathlib import Path

# Pure python script for rendering an index of format (Concept, Book #, Pg #, Notes) into an HTML document
def load_csv_file(filepath: str):
    # Initialize output
    out = None
    if not Path(filepath).exists():
        print(f"Could not find a file at {filepath}, exiting...")
        sys.exit(1)
    # Load the file and return the dictreader
    with open(filepath, "r", newline="") as csvfile:
        reader = csv.reader(csvfile)
        allrows = [row for row in reader]
        return allrows[0], allrows[1:] # split out header and body

def alphabetize(index: List[List[str]]):
    index.sort(key=lambda i: i[0].lower())

def add_colspan_header(htmlfile, header):
    """ write an extra row to the HTMLfile with a large letter indicating the next column """
    htmlfile.write(f'\n<tr align="left" id="subheader"><th colspan=4>{header}</th></tr>\n' )

def header_str_for(row):
    """ Returns a header string for the given row """
    letter: str = row[0][0]
    if letter in ascii_letters:
        return letter.upper()
    elif letter.isnumeric():
        return "#" 
    else:
        return "./?$!#"

def gen_html(headers, index, outfile):
    """ Generates HTML for the index and prints it to the console"""
    with open(outfile, "w") as htmlfile:
        # Write the HTML headers and 
        htmlfile.write('<!DOCTYPE html>\n\n<html>\n<head>\n<title>SANS Index</title>\n<link rel="stylesheet" href="sans.css"></head>\n')
        htmlfile.write("<body>\n<table>\n")
        
        # Write the headers w/ THEAD to support repeating headers in prints
        # htmlfile.write('<THEAD>\n<tr align="center">\n<COLGROUP>\n<COLGROUP SPAN=4>\n<COLGROUP SPAN 4>')
        htmlfile.write('<THEAD>\n<tr align="center">\n')
        for header in headers:
            htmlfile.write(f'<th scope=col>{header}</th>\n')
        htmlfile.write("</tr>\n</THEAD>\n")
        # Write the first header row
        current_header = ""
        # Write the index itself
        for entry in index:
            if header_str_for(entry) != current_header:
                current_header = header_str_for(entry)
                add_colspan_header(htmlfile, current_header)
            htmlfile.write("<tr align='left'>\n")
            # htmlfile.write("\n\t".join(f'<td>{c}</td>' for c in entry))
            htmlfile.write("\n\t<td>{}</td>\n\t<td align='center'>{}</td>\n\t<td align='center' >{}</td>\n\t<td>{}</td>\n".format(*entry))
            htmlfile.write("</tr>\n")
        htmlfile.write("</body>\n</table>\n</html>")
        

def main():
    if len(sys.argv) != 3:
        print("Usage: sansindex.py [csvfile] [outfile]\n[csvfile] should have four columns (Name, Book, Pg, Notes).\nFirst row will be interpreted as headers")
        sys.exit(1)
    headers, index = load_csv_file(sys.argv[1])
    alphabetize(index) # edits in-place
    gen_html(headers, index, sys.argv[2])
    webbrowser.open_new(sys.argv[2])
    sys.exit(0)

if __name__ == "__main__":
    main()