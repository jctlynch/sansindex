# SANS Index Converter

This is a quick and dirty script that converts a CSV file with four headers ("Name", "Book", "Page", and "Notes" in my use case) into an alphabetized HTML table. It saves the table to the second CLI argument, and opens a webbrowser via the Python `webbrowser` module to render the completed product

## Usage
`python3 sansindex.py \[csvfile\] \[outfile\]`